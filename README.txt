Le projet liste de courses doit permet de gérer des listes de courses, de leurs création à leurs utilisation. L'application devra permettre de 
créer un groupe et de rajouter des personnes dedans afin que les listes de courses soient visibles et modifiable par tous. Il pourra avoir 
plusieurs listes de courses et être nommée en conséquence par celui qui créer la liste. Chaqu'une des liste sera modifiable par toutes personnes
dans le groupe et la personne qui fera les courses pourra imprimer la liste de course ou alors la conserver sous format numérique. Dans 
chaqu'un des listes, on pourra trier les éléments par catégorie ou avec d'autres critères comme le prix. On pourras aussi afficher un prix 
moyen du panier en fonction de l'enseigne choisi. Afin de préparer ce projet, nous avons rédiger des user stories et les cas d'utilisations
pour préciser notre vision du projets et avoir une meilleur direction et efficacité.